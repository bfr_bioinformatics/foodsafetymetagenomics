# FoodSafetyMetagenomics

Collection of scripts for analysis of food metagenomic samples. 

Related publication "Fishing in the soup – Pathogen detection in food safety using Metagenomic sequencing".


## Overview


![Overview of proposed workflow](workflow.png)


### Provided scripts

* [kraken2_classification.sh](scripts/kraken2_classification.sh): 
Run kraken2 and convert result to valid taxonomy
* [kraken2_filtering.sh](scripts/kraken2_filtering.sh): 
Filter kraken2 results for matches to ABSA classified pathogens
*  [kraken2_read_extraction.sh](scripts/kraken2_read_extraction.sh):
extract reads of set of species from kraken2 report
* [kraken2_verification.sh](scripts/kraken2_verification.sh):
Verify kraken2 classifies reads with BLAST


## Installation of software dependencies
The set of scripts depend on metaphlan2,kraken2,seqtk,blast,bbmap,taxonkit,mash and srst2
The easiest way for installation of all dependencies is via [conda](https://docs.conda.io/en/latest/miniconda.html)

```sh
conda create -n foodpathogen_metagenomics metaphlan2 kraken2 seqtk blast bbmap taxonkit mash srst2
```

You can also use the provided [file](env/conda_env.yaml)
```sh
conda env create -n foodpathogen_metagenomics --file=path/2/file
```


## Databases
* ABSA database 
The ABSA database (https://my.absa.org/Riskgroups) was accessed on May 2, 2018 and processsed into a tabular format. The data is available [here](data/ABSA_tabular.csv)




* [minikraken2](ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken2_v1_8GB_201904_UPDATE.tgz)

* For the analysis of foodborne pathogens in metagenomic samples we suggest building a [custom kraken database](https://ccb.jhu.edu/software/kraken2/index.shtml?t=manual#custom-databases) with the set of all [ncbi refseq genomes](https://ftp.ncbi.nlm.nih.gov/refseq/release/complete/) including all available eukaryotic host species.

* taxonkit requires NCBI's taxonomy files (e.g. names.dmp and nodes.dmp).

* [mash database](https://gembox.cbcb.umd.edu/mash/refseq.genomes.k21s1000.msh)

* How to create a VFDB database for srst2 is explained [here](https://github.com/katholt/srst2#using-the-vfdb-virulence-factor-database-with-srst2).


----------------

## Example execution

* We Provide a small test set in the release for testing the workflow: [read1](https://gitlab.com/bfr_bioinformatics/foodsafetymetagenomics/uploads/60dd989c0bd73170dbea4166f5c2b479/testdata_R1.fastq.gz) and [read2](https://gitlab.com/bfr_bioinformatics/foodsafetymetagenomics/uploads/8459271cbdcb2b914f70d0403e2d04d8/testdata_R2.fastq.gz)
* The example data contains 200,000 reads among them 422 verified F. tulaensis reads
* Using your own data we recommend trimming the reads prior to the analysis, with e.g. [fastp](https://github.com/OpenGene/fastp).

0. Define data and db

* read1
* read2
* kraken_db
* absa_db
* outdir




1. Classify reads with kraken2

```
kraken2_classification.sh --read1 --read2 --translate --outdir --db
```

See `kraken2_classification.sh --help` for more details.

2. Filter kraken2 output with ABSA DB


```sh
kraken2_filtering.sh --input path/2/kraken2.mpareport --db path/2/absadb --outdir path/2/outdir
```


--input: Path to kraken mpa report

Options:
--db path/2/db
--outdir: Path to existing outDir (default: location of mpafile)



See `kraken2_filtering.sh --help` for more details.

3. Read extraction from filtered kraken2 results 

```sh

kraken2_read_extraction.sh

--read1: Path to read1 file (read1.fastq.gz)
--read2: Path to read2 file (read2.fastq.gz)
--species_list: Path to species_list (format: species   count   class)
--translate_file: Path to kraken.translate file


```


See `kraken2_read_extraction.sh --help` for more details.

 
4. Verify kraken2 classified reads after filtering using BLAST


```sh
Call: kraken2_verification.sh --fasta path/2/sample/fasta [Options]

Options:
--fasta: Path to fasta file
--reads2verify: number of reads to verify
--outdir
```

See `kraken2_verification.sh --help` for more details.




### Optional steps

1. mash screen

```sh
mash screen -w mash_dbreference <(cat path/2/read1.fastq path/2/read1.fastq) -p threads > path/2/mashresults/sample_mashscreen_winner.tab
```

2. virulence factor search


* Set up vfdb for a specific genus (see [here](https://github.com/katholt/srst2#using-the-vfdb-virulence-factor-database-with-srst2))

* run command in strict criteria

```sh
srst2 --input_pe readdir/*.fastq --output path/2/dir/vfdb --log --gene_db path/2/db --threads 24 --keep_interim_alignment
```

Note: You might need to specify ` --forward ` and `--reverse _R2_ftularensis` according to your read format.

* run command with relaxed criteria

```sh
srst2 --input_pe readdir/*.fastq --output path/2/dir/vfdb --log --gene_db path/2/db --threads 24 --keep_interim_alignment --min_coverage 60 --min_depth 2
```
Note: You might need to specify ` --forward ` and `--reverse _R2_ftularensis` according to your read format. Note also that you may change the settings here to reflext your expected gene coverage depth.

## References

* ABSA: https://my.absa.org/Riskgroups
* kraken2: https://github.com/DerrickWood/kraken2
* mash: https://github.com/marbl/Mash
* srst2: https://github.com/katholt/srst2
* VFDB: http://www.mgc.ac.cn/VFs/
* bbmap: https://github.com/BioInfoTools/BBMap, https://sourceforge.net/projects/bbmap/
* NCBI database: https://ftp.ncbi.nlm.nih.gov/refseq/release/
* BLAST: https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download
* metaphlan2: https://bitbucket.org/biobakery/metaphlan2
* seqtk: https://github.com/lh3/seqtk
