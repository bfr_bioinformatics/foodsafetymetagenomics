#!/bin/bash
set -e
set -u
set -o pipefail

#Goal: Verify kraken2-classification with extracted reads 
#Author: Carlus Deneke, Carlus.Deneke@bfr.bund.de; Josephine Gruetzke, Josephine.Gruetzke@bfr.bund.de
version=0.1

#Defaults
fasta="NA"
reads2verify=false
name=false
minlength=0.9 
interactive=false
force=false
help=false
verbose=false
outdir=false
prefix=false
max_target_seqs=500
max_hsps=500

OPTIONS=hf:n:m:p:o:ivt:a:s:
LONGOPTIONS=help,fasta:,reads2verify:,minlength:,prefix:,outdir:,interactive,verbose,max_target_seqs:,max_hsps:,species:

## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type blastn_verification_kraken2.sh --help"
fi

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"


while true; do
    case "$1" in
        -h|--help)
            help=true
            shift
            ;;
        -f|--fasta)
            fasta="$2"
            shift 2
            ;;
        -n|--reads2verify)
            reads2verify="$2"
            shift 2
            ;;
        -p|--prefix)
            prefix="$2"
            shift 2
            ;;
        -s|--species)
            name="$2"
            shift 2
            ;;
        -o|--outdir)
            outdir="$2"
            shift 2
            ;;
        -m|--minlength)
            minlength="$2"
            shift 2
            ;;
        -t|--max_target_seqs)
            max_target_seqs="$2"
            shift 2
            ;;
        -a|--max_hsps)
            max_hsps="$2"
            shift 2
            ;;
        --interactive)
            interactive=true
            shift
            ;;
        -v|--verbose)
            verbose=true
            shift
            ;;
        -f|--force)
            force=true
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# call help ------------------------------------------
if [[ $help == true ]]
then
    echo "You called the script blastn_verification_kraken2.sh"
    echo
    echo "============================="
    echo "Call: kraken2_verification.sh --fasta path/2/sample/fasta [Options]"
    

    echo 
    echo "Options:"
    echo "--fasta: Path to fasta file"
    echo "--reads2verify: number of reads to verify "
    echo "--minlength: proportion of readlength that must be aligned to blastn hit for verification (default: 0.9)"
    echo "--species: species to verify; format: Salmonella_enterica (default prefix of fasta file)"
    echo "--outdir: Path to existing outDir (default:../fasta)"
    echo "--max_target_seqs: (default 500)"
    echo "--max_hsps: (default 500)"
    echo "--prefix: Prefix of output file  (default: prefix of fasta file)"
    echo "--interactive: Ask before starting the program"
    echo "--verbose: Ask before starting the program"
    echo "--force: Overwrite existing files in outdir"
    echo "--help: Display this help message"


    echo "============================="
    exit 1

fi


# Definitions 
species=`echo ${name/_/ }`

if [[ $prefix == false ]]; then
    prefix=`basename $fasta .fasta`
fi

if [[ $outdir == false ]]; then
    fastadir=`dirname $fasta`
    parentdir=`dirname $fastadir`
    outdir=${parentdir}/blast_confirmation/${prefix}
fi

if [[ $name == false ]]; then
    species=`echo ${prefix/_/ }`
fi

if [[ $reads2verify == false ]]; then
    reads2verify=`grep -c ">" $fasta`
fi


blast_results=$outdir/${prefix}_blast.tsv
confirmation_stats=$outdir/${prefix}_confirmation_stats.tsv
head=$(($reads2verify * 2))

# ask if in interactive mode -----------------------------

echo "fasta: $fasta"
echo "reads2verify: $reads2verify"
echo "minlength: $minlength"
echo "max_target_seqs: $max_target_seqs"
echo "max_hsps: $max_hsps"
echo "prefix: $prefix"
echo "species: $name"
echo "outdir: $outdir"
echo "verbose: $verbose"
echo "force: $force"


if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi

# input file exists
if [[ $fasta == "NA" ]]; then
    echo "Please specify input file --fasta"
    exit 1
fi

if [[ ! -f $fasta ]]; then
    echo "File $fasta does NOT exist"
    exit 1
fi


# output File already exists
if [[ -f $blast_results && force == false ]];then
    echo "File $output_file already exists. Use --force to overwrite"
    exit 1
fi

# blastn with fasta file------------------------------------------------
echo "Start running blastn_verification_kraken2.sh"
mkdir -p $outdir
echo -e "name\treads_total\treads_unaligned\treads_aligned\treads_filtered\treads_significant\treads_falsepositives\trate_falsepositives\treads_confirmed\trate_confirmed" > $confirmation_stats

echo "blastn started with $fasta"
blastn -db nt -outfmt "6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen sscinames scomnames stitle staxids" -query <(head -n $head $fasta)  -out $blast_results -max_target_seqs $max_target_seqs -max_hsps $max_hsps -num_threads 5
echo "blastn done for $fasta"

# keep best hsp
echo "extract best hsp"
cut -f 1-2 $blast_results | sort | uniq | while read pattern; do
    grep -m 1 "$pattern" $blast_results >> $outdir/${prefix}_blast_besthsp.tsv
done

# keep only alignments /hsps) longer than 0.9% of query length
awk -F'\t' -v minlength=$minlength '$4/$13 > minlength' $outdir/${prefix}_blast_besthsp.tsv > $outdir/${prefix}_blast_besthsp_significant.tsv 

# keep only hits to desired target
grep "$species" $outdir/${prefix}_blast_besthsp_significant.tsv > $outdir/${prefix}_blast_besthsp_significant_2target.tsv


# stats

# * reads unaligned -> not from blast
reads_aligned=`cut -f 1 $blast_results | sort | uniq | wc -l`
#reads_total=`head -n $head $fasta | grep -c '>'`
reads_total=$reads2verify
reads_unaligned=`bc -l <<< $reads_total-$reads_aligned`

# * reads aligned but filtered out
reads_significant=`cut -f 1 $outdir/${prefix}_blast_besthsp_significant.tsv | sort | uniq | wc -l`
reads_filtered=`bc -l <<< $reads_aligned-$reads_significant`

# * reads confirmed
reads_confirmed=`grep "$species" $outdir/${prefix}_blast_besthsp_significant.tsv | cut -f 1 | sort | uniq | wc -l`

# * reads aligned only to other ref
reads_falsepositives=`bc <<< $reads_significant-$reads_confirmed`
rate_confirmed=`bc -l <<< $reads_confirmed/$reads_total`
rate_falsepositives=`bc -l <<< $reads_falsepositives/$reads_total`

echo -e "$species\t$reads_total\t$reads_unaligned\t$reads_aligned\t$reads_filtered\t$reads_significant\t$reads_falsepositives\t$rate_falsepositives\t$reads_confirmed\t$rate_confirmed" >> $confirmation_stats


