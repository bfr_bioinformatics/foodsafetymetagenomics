#!/bin/bash
set -e
set -u
set -o pipefail



#Goal: Extract reads for species specified in a text file from ABSA-filtering
#Author: Josephine Gruetzke, Josephine.Gruetzke@bfr.bund.de; Carlus Deneke, Carlus.Deneke@bfr.bund.de
version=0.2

#Defaults
fastq1="NA"
fastq2="NA"
translate="NA"
species_list="NA"
onespecies="NA"
onegenus="NA"
interactive=false
force=false
help=false
verbose=false
outdir=false
prefix=false

# Parser

OPTIONS=hr1:r2:p:o:ivsl:t:g:
LONGOPTIONS=help,read1:,read2:,prefix:,outdir:,interactive,verbose,species:,species_list:,translate_file:,genus:

## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type classified_read_extraction.sh --help"
fi

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi


# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

while true; do
    case "$1" in
        -h|--help)
            help=true
            shift
            ;;
        -r1|--read1)
            fastq1="$2"
            shift 2
            ;;
        -r2|--read2)
            fastq2="$2"
            shift 2
            ;;
        -t|--translate_file)
            translate="$2"
            shift 2
            ;;
        -l|--species_list)
            species_list="$2"
            shift 2
            ;;
        -s|--species)
            onespecies="$2"
            shift 2
            ;;
        -g|--genus)
            onegenus="$2"
            shift 2
            ;;
        -p|--prefix)
            prefix="$2"
            shift 2
            ;;
        -o|--outdir)
            outdir="$2"
            shift 2
            ;;
        -i|--interactive)
            interactive=true
            shift
            ;;
        -v|--verbose)
            verbose=true
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done



# call help ------------------------------------------
if [[ $help == true ]]
then
    echo "You called the script classified_read_extraction.sh"
    echo
    echo "============================="
    echo "Call: classified_read_extraction.sh --read1 path/2/read1.fastq.gz --read2 path/2/read2.fastq.gz --species_list path/2/pathogen-list.tsv [Options]"
    echo "--read1: Path to read1 file (read1.fastq.gz)"
    echo "--read2: Path to read2 file (read2.fastq.gz)"
    echo "--species_list: Path to species_list (format: species   count   class)"
    echo "--translate_file: Path to kraken.translate file"

    echo 
    echo "Options:"
    echo "--species: if no species list available specify species in quotation marks (format: 'Francisella tularensis')"
    echo "--genus: if no species list available specify genus in quotation marks (format: 'Francisella')"
    echo "--outdir: Path to existing outDir (default: $outdir)"
    echo "--prefix: Prefix of output file  (default: $prefix)"
    echo "--interactive: Ask before starting the program"
    echo "--verbose: Ask before starting the program"
    echo "--help: Display this help message"


    echo "============================="
    exit 1

fi


#TODO: filter
#filter bacteria, virus, eukaryota, archea

# Definitions 


# ask if in interactive mode -----------------------------



if [[ $outdir == false ]]; then
    outdir=`dirname $species_list`
fi


if [[ $prefix == false ]]; then
    prefix=`basename $translate .translate`
fi

echo "read1: $fastq1"
echo "read2: $fastq2"
echo "species_list: $species_list"
echo "species: $onespecies"
echo "genus: $onegenus"
echo "translate_file: $translate"
echo "outdir: $outdir"
echo "prefix: $prefix"
echo "verbose: $verbose"

if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi



# Checks  -----------------------------

# input read1 specified
if [[ $fastq1 == "NA" ]]; then
    echo "Please specify input read1 file --read1"
    exit 1
fi

# input read1 does not exist
if [[ ! -f $fastq1 ]];then
    echo "File $fastq1 does NOT exist"
    exit 1
fi

# input read2 specified
if [[ $fastq2 == "NA" ]]; then
    echo "Please specify input read2 file --read2"
    exit 1
fi

# input read2 does not exist
if [[ ! -f $fastq2 ]];then
    echo "File $fastq2 does NOT exist"
    exit 1
fi
# input species_list specified
if [[ $species_list == "NA" && $onespecies == "NA" && $onegenus == "NA" ]]; then
    echo "Please specify pathogen list --species_list, species --species or genus --genus"
    exit 1
fi

# species_list does not exist
if [[ $species_list != "NA" && ! -f $species_list ]]; then
    echo "File $species_list does NOT exist"
    exit 1
fi

# input translate_file specified
if [[ $translate == "NA" ]]; then
    echo "Please specify kraken.translate file --translate_file"
    exit 1
fi

# translate_file does not exist
if [[ ! -f $translate ]]; then
    echo "File $translate does NOT exist"
    exit 1
fi


# Extract reads using kraken2.translate and pathogen list from ABSA_filtering_kraken2.sh -----------------------------

echo "Start running classified_read_extraction.sh"

source activate bioinfo
mkdir -p $outdir/$prefix
mkdir -p $outdir/tmp

read_file1=$outdir/tmp/${prefix}_R1.fastq
read_file2=$outdir/tmp/${prefix}_R2.fastq

zcat $fastq1 > $read_file1
zcat $fastq2 > $read_file2


if [[ $species_list != "NA" ]] ; then
while IFS=$'\t' read species count class; do

if [[ $verbose == true ]]; then echo "extract reads for species $species"; fi

    species=`echo $species | tr -d '[0-9]'`
    name=`echo ${species// /_}`
    (grep "s__${species}" $translate  || echo "") | cut -f1 > $outdir/${prefix}/${name}_extractedReadIDs.txt
    #(grep "s__${species}" $translate | cut -f1) || echo "" > $outdir/${prefix}/${name}_extractedReadIDs.txt
    extractedReadIDs=$outdir/${prefix}/${name}_extractedReadIDs.txt
    filterbyname.sh in=$read_file1 in2=$read_file2 out=$outdir/$prefix/${name}_R1.fastq out2=$outdir/$prefix/${name}_R2.fastq names=$extractedReadIDs include=t ow=t
    seqtk seq -A $outdir/${prefix}/${name}_R1.fastq > $outdir/${prefix}/${name}.fasta
    seqtk seq -A $outdir/${prefix}/${name}_R2.fastq >> $outdir/${prefix}/${name}.fasta
done < "$species_list"
fi

if [[ $onespecies != "NA" ]] ; then
if [[ $verbose == true ]]; then echo "extract reads for species $onespecies"; fi

    species=`echo $onespecies | tr -d '[0-9]'`
    name=`echo ${species// /_}`
    (grep "s__${species}" $translate  || echo "") | cut -f1 > $outdir/${prefix}/${name}_extractedReadIDs.txt
    #(grep "s__${species}" $translate | cut -f1) || echo "" > $outdir/${prefix}/${name}_extractedReadIDs.txt
    extractedReadIDs=$outdir/${prefix}/${name}_extractedReadIDs.txt
    filterbyname.sh in=$read_file1 in2=$read_file2 out=$outdir/$prefix/${name}_R1.fastq out2=$outdir/$prefix/${name}_R2.fastq names=$extractedReadIDs include=t ow=t
    seqtk seq -A $outdir/${prefix}/${name}_R1.fastq > $outdir/${prefix}/${name}.fasta
    seqtk seq -A $outdir/${prefix}/${name}_R2.fastq >> $outdir/${prefix}/${name}.fasta
fi

if [[ $onegenus != "NA" ]] ; then
if [[ $verbose == true ]]; then echo "extract reads for genus $onegenus"; fi

    genus=`echo $onegenus | tr -d '[0-9]'`
    name=`echo ${genus// /_}`
    (grep "g__${genus}" $translate  || echo "") | cut -f1 > $outdir/${prefix}/${name}_extractedReadIDs.txt
    extractedReadIDs=$outdir/${prefix}/${name}_extractedReadIDs.txt
    filterbyname.sh in=$read_file1 in2=$read_file2 out=$outdir/$prefix/${name}_R1.fastq out2=$outdir/$prefix/${name}_R2.fastq names=$extractedReadIDs include=t ow=t
    seqtk seq -A $outdir/${prefix}/${name}_R1.fastq > $outdir/${prefix}/${name}.fasta
    seqtk seq -A $outdir/${prefix}/${name}_R2.fastq >> $outdir/${prefix}/${name}.fasta
fi

