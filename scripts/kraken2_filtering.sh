#!/bin/bash
set -e
set -u
set -o pipefail


#Goal: Filter kraken2 mpareport output with ABSA DB
#Author: Carlus Deneke, Carlus.Deneke@bfr.bund.de; Josephine Grützke, Josephine.Gruetzke@bfr.bund.de
version=0.2


#Defaults
strict=false
mismatches=2
#ABSAdb="data/ABSA_tabular.csv"
ABSAdb="NA"
filter_HP=false
filter_AP=false
filter_PP=false
filter_CDC=false
filter_USDA=false
mincount=0
mpa_file="NA"
help=false
verbose=false
outdir=false
prefix=false
interactive=false

# Parser

OPTIONS=hd:i:p:o:fm:v
LONGOPTIONS=help,db:,input:,prefix:,outdir:,interactive,force,mincount:,HP,AP,PP,CDC,USDA,strict,mismatches:,verbose
#domain


## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type create_sampleSheet.sh --help"
fi



PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"


while true; do
    case "$1" in
        -h|--help)
            help=true
            shift
            ;;
        -d|--db)
            ABSAdb="$2"
            shift 2
            ;;
        -i|--input)
            mpa_file="$2"
            shift 2
            ;;
        -p|--prefix)
            prefix="$2"
            shift 2
            ;;
        -o|--outdir)
            outdir="$2"
            shift 2
            ;;
        -m|--mincount)
            mincount="$2"
            shift 2
            ;;
        --interactive)
            interactive=true
            shift
            ;;
        -v|--verbose)
            verbose=true
            shift
            ;;
        --strict)
            strict=true
            shift
            ;;
        --HP)
            filter_HP=true
            shift
            ;;
        --AP)
            filter_AP=true
            shift
            ;;
        --PP)
            filter_PP=true
            shift
            ;;
        --CDC)
            filter_CDC=true
            shift
            ;;
        --USDA)
            filter_USDA=true
            shift
            ;;
        -f|--force)
            force=true
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done






# call help ------------------------------------------
if [[ $help == true ]]
then
    echo "You called the script ABSA_filtering_kraken2.sh"
    echo
    echo "============================="
    echo "Call: ABSA_filtering_kraken2.sh --input path/2/kraken/mpareport [Options]"
    echo "--input: Path to kraken mpa report"
    echo "--db path/2/absadb located in data/ABSA_tabular.csv relative to the git repository"

    echo 
    echo "Options:"

    echo "--outdir: Path to existing outDir (default: location of mpafile)"
    echo "--prefix: Prefix of output file  (default: prefix of mpafile)"
    echo "--interactive: Ask before starting the program"
    echo "--verbose: Ask before starting the program"
    echo "--force: Overwrite existing samples.tsv files in outdir"
    echo "--help: Display this help message"

    echo "--HP Filter for human pathogens"
    echo "--AP  Filter for animal pathogens"
    echo "--PP  Filter for plant pathogens"
    echo "--CDC  Filter for pathogens according to CDC select agents"
    echo "--USDA  Filter for pathogens according to USDA select agents"
    echo "--strict Use grep (fixed pattern) instead of tre-agrep (fuzyy matching). Fuzzy search can find misspelled species names in the ABSA DB"

    echo
    echo "For more details, also see ABSA database (https://my.absa.org/Riskgroups)"

    echo "============================="
    exit 1

fi



# Definitions 


if [[ $outdir == false ]]; then
    outdir=`dirname $mpa_file`
fi


if [[ $prefix == false ]]; then
    #prefix=`basename $mpa_file | cut -f 1 -d "."`
    prefix=`basename -s .mpareport $mpa_file`
fi

output_file=$outdir/${prefix}_absa.tsv
pathogen_HP_file=$outdir/${prefix}_absa_HP.tsv
pathogen_AP_file=$outdir/${prefix}_absa_AP.tsv
pathogen_PP_file=$outdir/${prefix}_absa_PP.tsv
pathogen_CDC_file=$outdir/${prefix}_absa_CDC.tsv
pathogen_USDA_file=$outdir/${prefix}_absa_USDA.tsv

# ask if in interactive mode -----------------------------

echo "ABSAdb: $ABSAdb"
echo "mpa_file: $mpa_file"
echo "outdir: $outdir"
echo "prefix: $prefix"
echo "output_file: $output_file"
#echo "pathogen_HP_file: $pathogen_HP_file"
echo "filter_HP: $filter_HP"
echo "filter_AP: $filter_AP"
echo "filter_PP: $filter_PP"
echo "filter_CDC: $filter_CDC"
echo "filter_USDA: $filter_USDA"
echo "strict: $strict"
echo "mismatches: $mismatches"
echo "mincount: $mincount"
echo "verbose: $verbose"


if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi



# Checks  -----------------------------
# ABSA exists
if [[ ! -f $ABSAdb ]];then
    echo "ABSAdb $ABSAdb does NOT exist"
    exit 1
fi

# input file exists
if [[ $mpa_file == "NA" ]]; then
    echo "Please specify input file --input"
    exit 1
fi

if [[ ! -f $mpa_file ]]; then
    echo "File $mpa_file does NOT exist"
    exit 1
fi


# output File already  exists
if [[ -f $output_file && force == false ]];then
    echo "File $output_file already exists. Use --force to overwrite"
    exit 1
fi


# check existence of tre-agrep
if ! [ -x "$(command -v tre-agrep)" ]; then
  echo 'Error: tre-agrep is not installed. Use --strict to perform a strict grep search'
  exit 1
fi


# Match with ABSA pathogen info from kraken2 mpa-report -----------------------------

echo "Start running ABSA_filtering_kraken2.sh"

echo -e "Kraken_species\tKraken_count\tClass\tName\tHP\tAP\tPP\tselect_cdc\tselect_usda" > $output_file



while IFS=$'\t' read name count; do
    species=`echo $name | awk -F's__' '{print $2}'`
    domain=`echo $name | cut -f 1 -d '|' | sed 's/d__//g'`


    #TODO domain filter
    # Domain filter
    #Archaea
    #Bacteria
    #Eukaryota
    #Viruses
    #if [[ $filter_domain ]];then echo "OK"; fi
    
    if [[ $verbose == true ]]; then echo "Processing entry $name for species $species"; fi


    # exceptions Salmonella
    if [[ $species == "Salmonella enterica" ]]; then
        search="Salmonella typhimurium"
    else
        search="$species"
    fi 

    #classification_default="\"$domain\"\t\"NA\"\t\"NA\"\t\"NA\"\t\"NA\"\t\"NA\"\t\"NA\""
    classification_default="$domain\tNA\tNA\tNA\tNA\tNA\tNA"

    # fuzzy or strict grep
    if [[ $strict == true ]]; then
        classification=`(grep "$search" $ABSAdb | tr ',' '\t' | head -n 1 | sed 's/\"//g') || echo "$classification_default"`
    else
        classification=`(tre-agrep -$mismatches "$search" $ABSAdb | tr ',' '\t' | head -n 1 | sed 's/\"//g') || echo "$classification_default"`
    fi


    # write output
    echo -e "$species\t$count\t$classification" >> $output_file

done < <(cat $mpa_file | grep 's__' | awk -F'\t' -v count_thr=$mincount '$2 > count_thr' | sort -nr -k 2 -t $'\t')


echo "Done writing file $output_file"

# Filtering ------------------------------------------------------------

# filter output for HP
if [[ $filter_HP == true ]]; then
    echo -e "Kraken_species\tKraken_count\tClass" > $pathogen_HP_file
    awk -F'\t' '$5 ~ "y" {print $1"\t"$2"\t"$3}' $output_file >> $pathogen_HP_file
    echo "Done writing file $pathogen_HP_file"
fi


# filter output for AP
if [[ $filter_AP == true ]]; then
    echo -e "Kraken_species\tKraken_count\tClass" > $pathogen_AP_file
    awk -F'\t' '$6 ~ "y" {print $1"\t"$2"\t"$3}' $output_file >> $pathogen_AP_file
    echo "Done writing file $pathogen_AP_file"
fi

# filter output for PP
if [[ $filter_PP == true ]]; then
    echo -e "Kraken_species\tKraken_count\tClass" > $pathogen_PP_file
    awk -F'\t' '$7 ~ "y" {print $1"\t"$2"\t"$3}' $output_file >> $pathogen_PP_file
    echo "Done writing file $pathogen_PP_file"
fi

# filter output for CDC_select
if [[ $filter_CDC == true ]]; then
    echo -e "Kraken_species\tKraken_count\tClass" > $pathogen_cdc_file
    awk -F'\t' '$8 ~ "y" {print $1"\t"$2"\t"$3}' $output_file >> $pathogen_cdc_file
    echo "Done writing file $pathogen_cdc_file"
fi

# filter output for USDA_select
if [[ $filter_USDA == true ]]; then
    echo -e "Kraken_species\tKraken_count\tClass" > $pathogen_usda_file
    awk -F'\t' '$9 ~ "y" {print $1"\t"$2"\t"$3}' $output_file >> $pathogen_usda_file
    echo "Done writing file $pathogen_usda_file"
fi



echo "Done parsing kraken2 mpareport with ABSA DB on `date`"
