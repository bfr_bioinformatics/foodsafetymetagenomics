#!/bin/bash
set -e
set -u
set -o pipefail


# TODO test


prefix="NA"
force=false
krona=false
threads=1
read1="NA"
read2="NA"
taxonDB="NA"


OPTIONS=hr1:r2:p:o:ivsl:t:g:
LONGOPTIONS=help,read1:,read2:,prefix:,outdir:,interactive,verbose,species:,species_list:,translate_file:,genus:

## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type classified_read_extraction.sh --help"
fi

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi


# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

while true; do
    case "$1" in
        -h|--help)
            help=true
            shift
            ;;
        -r1|--read1)
            fastq1="$2"
            shift 2
            ;;
        -r2|--read2)
            fastq2="$2"
            shift 2
            ;;
        --db)
            RefDB="$2"
            shift 2
            ;;
        --taxon_db)
            taxonDB="$2"
            shift 2
            ;;
        -t|--translate)
            translate=true
            shift
            ;;
        --krona)
            krona=true
            shift
            ;;
        -p|--prefix)
            prefix="$2"
            shift 2
            ;;
        -o|--outdir)
            outdir="$2"
            shift 2
            ;;
        -t|--threads)
            threads="$2"
            shift 2
            ;;
        -i|--interactive)
            interactive=true
            shift
            ;;
        -f|--force)
            force=true
            shift
            ;;
        -v|--verbose)
            verbose=true
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done




## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type PubMLST_Download_Alleles.sh --help"
    exit
fi


## search for "--help" flag
if [[ $@ =~ '--help' || $@ =~ '-h' ]]; then
    echo "Help file ----------------------"
    echo
#    echo "Basic usage: kraken2_classification.sh outdir sampleList refdb [options]"

    echo "Mandatory positional arguments"
    echo -e "--outdir\tdirectory where output should be written to (e.g. somepath/kraken2)"
    echo -e "--read1\tfull path to  read1"
    echo -e "--read2\tfull path to  read2"
    echo -e "--db\tfull path to kraken2 refdb directory"
    echo
    echo "Optional arguments (place after positional arguments)"
    echo -e "--verbose\tMore verbose output"
    echo -e "--interactive\tConfirm before"
    echo -e "--force\tForce overwrite if output file already exists"
    echo -e "--threads\t Number of threads to use in kraken2"
    echo -e "--translate\tCreate translated kraken file with taxonomic labels for each read"

    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi    




#taxonDB="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/taxonomy/"


# infer sample from read1 name or use prefix
if [[ $prefix == "NA" ]];then
    sample=$prefix
else
    sample=`basename $read1 | cut -f 1 -d '_'`
fi


# ----------------------

# Echo

echo
echo "Parameters:"
echo "outdir:$outdir"
echo "sample=$sample"
echo "read1=$read1"
echo "read2=$read2"
echo "RefDB:$RefDB"
echo "threads:$threads"
echo "taxonDB:$taxonDB"
echo "force: $force"
echo "verbose: $verbose"
echo "interactive: $interactive"
echo "translate: $translate"
echo "krona: $krona"

echo


# ask if in interactive mode
if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi


# ---------------------
# Checks

## check existence
if [ -d $RefDB ]
then
    echo "RefDB Dir exists for $RefDB"
else
    echo "Dir ${RefDB} does not exist."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


if [ ! -f $read1 ]
then
    echo "File ${read1} does not exist."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

if [ ! -f $read2 ]
then
    echo "File ${read2} does not exist."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

if [ -d $outdir ]
then
    echo "Project dir already exists"
    
else
    echo "Dir ${project_dir} does not exist."
    mkdir -p $outdir
fi



# ---------------------

echo "Started Kraken analysis on `date`"

# define output files

echo "Kraken analysis of sample $sample on `date`"

krakenfile="$sample.kraken"
krakenreport="$sample.report"
krakenmpareport="$sample.mpareport"
kronatext="$sample.krona.txt"
kronafile="$sample.krona.html"
biomfile="$sample.biom"
log="$outdir/$sample.log"
abundance_file_species="$sample.SpeciesAbundance.bracken"
abundance_file_genus="$sample.GenusAbundance.bracken"
translatefile="$sample.translate"
tmpfile="$sample.tmp"



# run kraken

#TODO add skip option
if [[ -f $outdir/$krakenfile && $force == false ]]; then
    echo "Output $outdir/$krakenfile already exists. Skip kraken2"
else
    echo "kraken2 --gzip-compressed --db $RefDB  --paired --threads $threads --output $outdir/$krakenfile --report $outdir/$krakenmpareport --use-mpa-style $read1  $read2"
    kraken2 --gzip-compressed --db $RefDB  --paired --threads $threads --output $outdir/$krakenfile --report $outdir/$krakenmpareport --use-mpa-style $read1  $read2
fi


# Convert kraken and create Krona Plots
if [[ $krona == true ]]; then
    ### Conversion of output mpa report into krona-accepted format
    echo "# Create Krona txt for $sample  on `date`" |& tee -a $log
    metaphlan2krona.py -p $outdir/$krakenmpareport -k $outdir/$kronatext

    ### Sort file for abundance
    echo "# Create Krona plot for $sample  on `date`" |& tee -a $log
    echo "source activate /home/DenekeC/anaconda3/envs/kraken"
    ktImportText $outdir/$kronatext -o $outdir/$kronafile
fi


### Add full lineage to kraken output
if [[ $translate == true ]]; then
    echo "source activate /home/DenekeC/anaconda3/envs/bioinfo"
    echo "# Create kraken translate file for $sample  on `date`" |& tee -a $log
    echo "paste <(cut $outdir/$krakenfile -f2) <(cut $outdir/$krakenfile -f3 | taxonkit lineage --data-dir $taxonDB | taxonkit reformat --data-dir $taxonDB --format \"k__{k}|p__{p}|c__{c}|o__{o}|f__{f}|g__{g}|s__{s}\" --miss-rank-repl \"unclassified\" | cut -f3)   > $outdir/$translatefile"
    paste <(cut $outdir/$krakenfile -f2) <(cut $outdir/$krakenfile -f3 | taxonkit lineage --data-dir $taxonDB | taxonkit reformat --data-dir $taxonDB --format "k__{k}|p__{p}|c__{c}|o__{o}|f__{f}|g__{g}|s__{s}" --miss-rank-repl "unclassified" | cut -f3)   > $outdir/$translatefile
fi



echo "Done running kraken2 on `date`."
